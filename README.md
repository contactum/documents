# Digital Contact Tracing Service: DCTS
## an improved decentralised design for privacy and effectiveness

We propose a decentralized digital contact tracing service that preserves the users' privacy and security by design. Our approach is based on Bluetooth and measures actual encounters of people, the contact time period, and estimates the proximity of the contact. We focus on preventing location tracking of users, protecting their data, and identity, while still tracing contacts and the possible spread of infectious diseases. We verify and improve the impact of tracking based on feedback provided by the use of epidemiological models. We compare a centralized and decentralized approach on a legal perspective and find a decentralized approach preferable considering proportionality and data minimization.

We improve existing concepts through the use of private set intersection which provides better privacy for the infected users. Additionally, we show a way to trace second order contacts in a decentralized system while protecting the privacy of the user. This approach also prevents attackers from uploading contacts without prior contact to an infected person.

We collaborate and develop together with [ITO](https://www.ito-app.org), the development is open-source on [github](https://github.com/ito-org).